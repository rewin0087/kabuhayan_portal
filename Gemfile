source 'https://rubygems.org'

gem 'modernizr-rails'
gem 'ejs'
gem 'underscore-rails' # for google map
gem 'gmaps4rails'
gem 'geocoder'
gem 'cancancan', '~> 1.10' # authorization
gem 'will_paginate', '~> 3.0.6' # pagination
gem 'friendly_id', '~> 5.1.0' # pretty url
gem 'bootstrap-wysihtml5-rails' # editor
gem 'rails', '4.1.10'
gem 'philippines', '~> 0.0.1.pre' # philippines location data
gem "select2-rails" # dropdown
gem 'devise' # authentication
gem "mini_magick" # image manipulation
gem 'omniauth-facebook' # authenticate with facebook
gem 'figaro' # environment config
gem 'rails-timeago', '~> 2.0' # lazy updating of time e.g. 4 minutes ago
gem 'bootstrap-sass', '~> 3.3'
gem 'websocket-rails'
gem 'ionicons-rails'
gem 'slim-rails'
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-turbolinks'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0',          group: :doc
gem 'thin', '~> 1.6.2'
gem 'airbrake'
gem 'rb-fsevent'
# eager loading optimization
# gem 'goldiloader'

# uploader and file storage
gem 'carrierwave'
gem 'carrierwave-dropbox'

group :production do
  ruby '2.3.0'
  # static assests
  gem 'rails_12factor'
  # Use postgresql as the database for Active Record
  gem 'pg'
  gem 'heroku_secrets', github: 'alexpeattie/heroku_secrets'
end

group :development do
  gem 'web-console', '~> 2.0'
  gem 'rack-mini-profiler'
  gem 'quiet_assets'
  gem 'mysql2'
  gem 'query_diet'
  gem 'terminal-notifier-guard'
end

group :development, :test do
  gem 'factory_girl_rails'
  gem 'ffaker'
  gem 'rspec-rails', '~> 3.1.0'
  gem 'rspec', '~> 3.1.0'
  gem 'pry-rails'
  gem 'guard'
  gem 'guard-rspec'
  gem 'spring'
  gem 'spring-commands-rspec'
  gem 'rspec-activemodel-mocks'
end

group :test do
  gem 'sqlite3'
  gem 'webmock'
  gem 'database_cleaner'
end