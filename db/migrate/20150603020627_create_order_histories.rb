class CreateOrderHistories < ActiveRecord::Migration
  def change
    create_table :order_histories do |t|
      t.string :status
      t.string :remark
      t.references :order, index: true

      t.timestamps
    end
  end
end
