class AddStatusToShop < ActiveRecord::Migration
  def change
    add_column :shops, :status, :string
  end
end
