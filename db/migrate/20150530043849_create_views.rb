class CreateViews < ActiveRecord::Migration
  def change
    create_table :views do |t|
      t.references :viewable, index: true
      t.string :viewable_type
      t.references :user, index: true

      t.timestamps
    end
  end
end
