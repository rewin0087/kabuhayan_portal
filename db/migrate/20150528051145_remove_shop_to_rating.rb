class RemoveShopToRating < ActiveRecord::Migration
  def change
    remove_reference :ratings, :shop, index: true
  end
end
