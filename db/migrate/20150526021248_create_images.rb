class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :file_name
      t.string :original_name
      t.references :product, index: true

      t.timestamps
    end
  end
end
