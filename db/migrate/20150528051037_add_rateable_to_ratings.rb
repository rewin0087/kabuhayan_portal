class AddRateableToRatings < ActiveRecord::Migration
  def change
    add_reference :ratings, :rateable, index: true
    add_column :ratings, :rateable_type, :string
  end
end
