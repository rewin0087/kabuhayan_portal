class CreatePins < ActiveRecord::Migration
  def change
    create_table :pins do |t|
      t.references :pinnable, index: true
      t.string :pinnable_type
      t.references :user, index: true

      t.timestamps
    end
  end
end
