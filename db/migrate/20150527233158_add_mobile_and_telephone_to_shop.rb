class AddMobileAndTelephoneToShop < ActiveRecord::Migration
  def change
    add_column :shops, :mobile, :string
    add_column :shops, :telephone, :string
  end
end
