class AddLatitudeAndLongitudeToTourism < ActiveRecord::Migration
  def change
    add_column :tourisms, :latitude, :float
    add_column :tourisms, :longitude, :float
  end
end
