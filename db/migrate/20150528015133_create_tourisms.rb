class CreateTourisms < ActiveRecord::Migration
  def change
    create_table :tourisms do |t|
      t.string :name
      t.text :description
      t.string :location
      t.string :category
      t.string :slug
      t.references :user, index: true

      t.timestamps
    end
  end
end
