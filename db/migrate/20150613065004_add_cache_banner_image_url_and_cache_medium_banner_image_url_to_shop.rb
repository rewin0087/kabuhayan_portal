class AddCacheBannerImageUrlAndCacheMediumBannerImageUrlToShop < ActiveRecord::Migration
  def change
    add_column :shops, :cache_banner_image_url, :string
    add_column :shops, :cache_medium_banner_image_url, :string
  end
end
