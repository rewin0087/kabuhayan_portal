class AddCacheMediumUrlAndCacheImageUrlToImage < ActiveRecord::Migration
  def change
    add_column :images, :cache_medium_url, :string
    add_column :images, :cache_image_url, :string
  end
end
