class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :remark
      t.string :status
      t.references :product, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
