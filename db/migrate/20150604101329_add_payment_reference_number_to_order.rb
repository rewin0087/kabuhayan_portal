class AddPaymentReferenceNumberToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :payment_reference_number, :string
  end
end
