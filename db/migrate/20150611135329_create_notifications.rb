class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :notifiable, index: true
      t.string :notifiable_type
      t.boolean :seen
      t.references :user, index: true
      t.integer :from_user

      t.timestamps
    end
  end
end
