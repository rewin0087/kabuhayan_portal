class RenameTypeToCategoryToRating < ActiveRecord::Migration
  def change
    rename_column :ratings, :type, :category
  end
end
