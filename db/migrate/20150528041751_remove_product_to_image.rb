class RemoveProductToImage < ActiveRecord::Migration
  def change
    remove_reference :images, :product, index: true
  end
end
