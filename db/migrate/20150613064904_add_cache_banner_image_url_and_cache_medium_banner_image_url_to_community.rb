class AddCacheBannerImageUrlAndCacheMediumBannerImageUrlToCommunity < ActiveRecord::Migration
  def change
    add_column :communities, :cache_banner_image_url, :string
    add_column :communities, :cache_medium_banner_image_url, :string
  end
end
