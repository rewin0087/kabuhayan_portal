module ApplicationHelper
  def locations
    cities = Philippines::PROVINCE_DATA.map {|p,v| v['cities'].map {|c| "#{c}, #{p}"}.flatten.sort }.flatten.sort

    locations = (cities + Philippines::MUNICIPALITIES_WITH_PROVINCE + Philippines::PROVINCES).sort
    locations.unshift(['',''])
  end

  def location_options(model)
    options_for_select locations, selected: model.try(:location)
  end

  def search_location_options(params)
    options_for_select locations, selected: params[:location]
  end

  def button_raters(model, user, disabled = {})
    render 'shared/button_raters', { model: model, user: user, disabled: disabled }
  end

  def remove_button(resource_url)
    render 'shared/remove_button', { resource_url: resource_url }
  end

  def load_more_button(resources, resources_url)
    render 'shared/load_more_button', { resources: resources, resources_url: resources_url }
  end

  def comment_form(model)
    render 'comments/form', { model: model }
  end

  def comments(model, show_form = true)
    render 'comments/list', { model: model, show_form: show_form }
  end

  def sanitize_text(text, max = 100)
    raw strip_tags(text)[0, max]
  end

  def search_form(search_path, search_placeholder = nil, location = nil)
    render 'shared/search_form', { search_path: search_path, search_placeholder: search_placeholder, location: location }
  end
end
