module ProductsHelper
  def category_options_for_product(product)
    categories = Product::CATEGORIES.map{|c| [c.gsub('_', ' ').humanize, c] }
    options_for_select categories, selected: product.try(:category)
  end

  def status_options_for_product(product)
    statuses = Product::STATUSES.map{|c| [c.gsub('_', ' ').humanize, c] }
    options_for_select statuses, selected: product.try(:status)
  end

  def location_options_for_product(product)
    options_for_select locations, selected: product.try(:location)
  end

  def currency_format(number)
    raw "&#8369;#{number_to_currency(number, unit: '')}"
  end

  def request_sent_to_buy?(product)
    current_user.orders.select{|o| o.product == product && o.status == 'request_sent'}.first
  end

  def order_in_processed?(product)
    not_allowed_status = %w(cancelled rejected delivery_received)
    current_user.orders.select{|o| o.product == product && !not_allowed_status.include?(o.status) }.first
  end
end
