module ShopsHelper
  def active?(i)
    'active' if i == 0
  end

  def category_options_for_shop(shop)
    categories = Shop::CATEGORIES.map{|c| [c.gsub('_', ' ').humanize, c] }
    options_for_select categories, selected: shop.try(:category)
  end

  def status_options_for_shop(shop)
    statuses = Shop::STATUSES.map{|c| [c.gsub('_', ' ').humanize, c] }
    options_for_select statuses, selected: shop.try(:status)
  end

  def shop_recent_label(current_user, shop)
    "#{@shop.user.full_name}'s Shops" unless shop.owner?(current_user)
  end
end
