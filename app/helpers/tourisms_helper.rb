module TourismsHelper
  def category_options_for_tourism(tourism)
    categories = Tourism::CATEGORIES.map{|c| [c.gsub('_', ' ').humanize, c] }
    options_for_select categories, selected: tourism.try(:category)
  end
end
