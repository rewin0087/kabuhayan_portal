module DeviseHelper
  def error_messages!(record = resource)
    return "" if record.errors.empty?

    message = record.errors.full_messages.first

    html = <<-HTML
    <div class="alert alert-error alert-block alert-dismissible" role="alert">
      <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
        <span aria-hidden='true'>&times;</span>
      </button>
      #{message}
    </div>
    HTML

    html.html_safe
  end

  def devise_error_messages!
    error_messages! resource
  end

  def devise_error_messages?
    resource.errors.empty? ? false : true
  end
end