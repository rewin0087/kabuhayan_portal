module PinsHelper
  def resource_icon(pinnable_type)
    case pinnable_type
    when 'community'
      'glyphicon glyphicon-blackboard bg-green'
    when 'product'
      'glyphicon glyphicon-tags bg-red'
    when 'tourism'
      'glyphicon glyphicon-map-marker bg-yellow'
    else
      ''
    end
  end

  def pinnable_image(pin, size = 400)
    case pin.type
    when 'community'
      # image_tag image_banner_path(pin.pinnable_resource.banner_image, size: size) unless pin.pinnable_resource.banner_image.blank?
      pin.pinnable_resource.banner_image_url unless pin.pinnable_resource.banner_image.blank?
    when 'product', 'tourism'
      # image_tag image_path(pin.pinnable_resource.images.first.file_name, size: size) unless pin.pinnable_resource.images.blank?
      pin.pinnable_resource.images.first.image_url unless pin.pinnable_resource.images.blank?
    else
      ''
    end
  end

  def pinnable_resource_path(pin)
    case pin.type
    when 'community'
      community_path(pin.pinnable_resource)
    when 'product'
      shop_product_path(pin.pinnable_resource.shop_id, pin.pinnable_resource)
    when 'tourism'
      tourism_path(pin.pinnable_resource)
    else
      ''
    end
  end
end