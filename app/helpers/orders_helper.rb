module OrdersHelper
  def status_highlighter(status)
    case status
    when 'request_sent' 
      'bg-maroon'
    when 'accepted'
      'bg-teal'
    when 'processing'
      'bg-yellow'
    when 'delivering'
      'bg-orange'
    when 'payed', 'delivered'
      'bg-aqua'
    when 'delivery_received'
      'bg-green'
    when 'denied'
      'bg-red'
    else
      'bg-black'
    end
  end

  def status_options_for_order(order, user)
    statuses = []
    index = 0

    if order.owner?(user)
      statuses = Order::STATUSES_AVAILABLE_FOR_OWNERS
      statuses = statuses - ['rejected'] if order.status != 'request_sent' 
    elsif order.buyer?(user)
      statuses = Order::STATUSES_AVAILABLE_FOR_BUYERS
      statuses = statuses - ['cancelled'] if order.status != 'request_sent' 
    end

    options_for_select statuses.map{|c| [c.gsub('_', ' ').humanize, c] }.unshift(['',''])
  end
end
