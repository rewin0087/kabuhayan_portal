module CommunitiesHelper
  def category_options_for_community(community)
    categories = Community::CATEGORIES.map{|c| [c.gsub('_', ' ').humanize, c] }
    options_for_select categories, selected: community.try(:category)
  end
end
