# encoding: utf-8

class BannerImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage ENV['CARRIER_WAVE_STORAGE'].to_sym
  # storage :file
  process resize_to_fit_with_contrast: [500, 400]

  version :medium do
    process resize_to_fit: [300, 300]
  end

  # set to local
  # FIXME: comment this if going to use file storage server
  def store_dir
    # Rails.root.join('uploads', 'banners')
    File.join('uploads', 'banners')
  end

  def filename
     "#{secure_token}#{File.extname(original_filename)}" if original_filename.present?
  end

  protected
    def secure_token(length=16)
      var = :"@#{mounted_as}_secure_token"
      model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
    end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  private
    def resize_to_fit_with_contrast(width, height)
      manipulate! do |img|
        img.combine_options do |c|
          c.sharpen   "0x1.2"
          c.quality   "70%"
          c.contrast
          c.fuzz      "90%"
          c.resize    "#{width}x#{height}>"
          c.resize    "#{width}x#{height}<"
        end

        img
      end
    end
end
