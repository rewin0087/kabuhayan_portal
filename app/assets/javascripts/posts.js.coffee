$(document).on 'page:load ready', () ->
  $('form#new_post').on 'ajax:complete', () ->
    $('.alert-error').remove()
    $(@)[0].reset()

  ws.unsubscribe('posts')
  postsChannel = ws.subscribe('posts')

  postsChannel.bind 'destroy', (post) ->
      $('div[data-post="' + post.raw.id + '"]').remove()

  postsChannel.bind 'new', (post) ->
    posts = $('#' + post.element).find('.inner-posts')
 
    if window.current_user == post.raw.user_id.toString() 
      posts.prepend(post.html.owner)
    else
      posts.prepend(post.html.public)
    $('time').timeago()