window.renderMap = (container, coords) ->
	handler = miniMap(container, coords)
	
	adjustMap(handler, container)
	
	$(window).resize () ->
		adjustMap(handler, container)

window.adjustMap = (handler, container) ->
	setTimeout () ->
		contentWrapperHeight = $('.content-wrapper').innerHeight() - 100
		contentHolderWidth = $('.slide-in .col-md-9').innerWidth() - 40
		holder = $('#' + container)
		holder.css({ height: contentWrapperHeight + 'px', width:  contentHolderWidth + 'px' })
		handler.getMap().setZoom(7)
		setTimeout () ->
			google.maps.event.trigger(handler.getMap(), 'resize')
		, 500
	, 1000

window.miniMap = (container, coords) ->
	handler = Gmaps.build('Google')
	philippines = new google.maps.LatLng(14.299018300000000000, 120.958969900000060000)

	provider = {
    center:    philippines,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles:    mapStyle
  }
  
	handler.buildMap { internal: { id: container }, provider: provider }, () ->
		markers = handler.addMarkers(coords)
		handler.bounds.extendWith(markers)
		handler.fitMapToBounds()

	handler

$(document).on 'page:load ready', () ->
  $('.links a').unbind()
  $('.links a').tooltip()

  