// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks
//= require select2
//= require rails-timeago
//= require bootstrap-sprockets
//= require admin_lte
//= require bootstrap-wysihtml5
//= require websocket_rails/main
//= require underscore
//= require gmaps/google
//= require base
//= require map_style
//= require_tree ../templates
//= require_tree .

