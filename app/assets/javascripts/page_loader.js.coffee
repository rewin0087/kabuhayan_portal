Turbolinks.enableProgressBar()

window.resetAnimation = () ->
  $('.content-wrapper .content').removeClass('bounceOutRight')
  $('.content-wrapper .content-header').removeClass('bounceOutUp')
  setTimeout () ->
    $('.content-wrapper .content-header').removeClass('bounceInDown')
  , 800
window.loader = (show) ->
  loader = $('.loader')

  if(show)
    loader.show()
  else
    loader.hide()

$(document).on 'page:fetch', (e) ->
  loader(true)

$(document).on 'page:receive page:restore', (e) ->
  loader(false)
  setTimeout () ->
    resetAnimation()
  , 300

$(document).ready () ->
  setTimeout () ->
    resetAnimation()
  , 300

$(document).on 'page:before-change', (e) ->
  $('.content-wrapper .content').addClass('bounceOutRight')
  $('.content-wrapper .content-header').addClass('bounceOutUp')