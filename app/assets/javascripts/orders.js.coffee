$(document).on 'page:load ready', () ->
  $('form#edit_order').on 'ajax:complete', () ->   
      $('.alert-error').remove()
      $(@)[0].reset()
      
    ws.unsubscribe('orders')
    ordersChannel = ws.subscribe('orders')

    ordersChannel.bind 'update', (order) ->
      element = $('#' + order.element)
      
      if window.current_user == order.raw.user_id.toString()
        element.replaceWith(order.html.buyer)
      else
        element.replaceWith(order.html.owner)
      $('time').timeago()
      $('.select-2').select2()