$(document).on 'page:load ready', () ->
  ws.unsubscribe('pins')
  pinsChannel = ws.subscribe('pins')

  pinsChannel.bind 'new', (pin) ->
    element = $('#' + pin.element)
    if window.current_user == pin.user.toString()
      element.prop('disabled', true)
    element.find('.inner').html(pin.pins)

  pinsChannel.bind 'destroy', (pin) ->
    element = $('#' + pin.element)
    if window.current_user == pin.user.toString()
      element.prop('disabled', false)
      element.removeClass('disabled')
    element.find('.inner').html(pin.pins)