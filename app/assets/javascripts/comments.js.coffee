$(document).on 'page:load ready', () ->
    $('form#new_comment').on 'ajax:complete', () ->   
      $('.alert-error').remove()
      $(@)[0].reset()
      
    ws.unsubscribe('comments')
    commentsChannel = ws.subscribe('comments')
    
    commentsChannel.bind 'destroy', (comment) ->
      $('div[data-comment="' + comment.raw.id + '"]').remove()
      
    commentsChannel.bind 'new', (comment) ->
      $('#' + comment.element + '-count').find('.inner').html(comment.comments)
      comments = $('#' + comment.element)

      innerComments = comments.find('.inner-comments')

      if window.current_user == comment.raw.user_id.toString() 
        $('form#new_comment')[0].reset()

      unless !innerComments.find('.comment[data-comment="' + comment.raw.id + '"]')
        
        if innerComments.find('.comment').length == 0
          if window.current_user == comment.raw.user_id.toString() 
            innerComments.html(comment.html.owner)
          else
            innerComments.html(comment.html.public)
        else
          if window.current_user == comment.raw.user_id.toString() 
            innerComments.append(comment.html.owner)
          else
            innerComments.append(comment.html.public)
        $('time').timeago()