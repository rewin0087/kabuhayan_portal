$(document).on 'page:load ready', () ->
  ws.unsubscribe('ratings')
  ratingsChannel = ws.subscribe('ratings')

  ratingsChannel.bind 'new', (rate) ->
    element = $('#' + rate.element)
    
    if window.current_user == rate.user.toString()
      if rate.raw.category == 'positive'
        element.addClass('primary-color')
      else
        element.addClass('secondary-color')

    element.find('.inner').html(rate.ratings)

  ratingsChannel.bind 'destroy', (rate) ->
    element = $('#' + rate.element)

    if window.current_user == rate.user.toString()
      if rate.raw.category == 'positive'
        element.removeClass('primary-color')
      else
        element.removeClass('secondary-color')

    element.find('.inner').html(rate.ratings)
