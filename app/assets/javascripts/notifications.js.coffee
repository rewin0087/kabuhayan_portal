$(document).on 'page:load ready', () ->
	ws.unsubscribe('notifications')
	notificationsChannel = ws.subscribe('notifications')

	notificationsChannel.bind 'new', (notification) ->
		if window.current_user == notification.raw.user_id.toString()
			$('.notifications-menu .count').html(notification.total)
			notifications = $('.notification-holder').find('ul.menu')
			notifications.prepend(notification.html)

			if $('body').hasClass('notifications')
				$('.notifications ul.list-group').prepend(notification.html)
				$('.notifications .count').html(notification.total)
			$('time').timeago()
			$('.links a').tooltip()
			_m.render_success(notification.content, notification.url)