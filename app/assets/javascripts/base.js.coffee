window.ws = new WebSocketRails(window.document.location.host + '/websocket')
window.current_user = $('body').attr('data-user')

$(document).on 'page:load ready', () ->
  ws.bind 'on_close', () ->
    console.log 'reconnecting'
    ws = new WebSocketRails(window.document.location.host + '/websocket')
  ws.bind 'on_open', () ->
    console.log 'connecting'

  $('[data-toggle="popover"]').popover();

  window.current_user = $('body').attr('data-user')
  $('.select-2').each (i,v) ->
    $(v).unbind()
  $('.select-2').each (i,v) ->
    $(v).select2()

  $.ajaxSetup({
    statusCode: {
      200: () ->
        $('time').timeago()
    }
  })