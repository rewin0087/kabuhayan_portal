class Tourism < ActiveRecord::Base
  extend FriendlyId
  include FriendlyParamHelper
  include PaginationHelper
  include OwnerHelper
  include ImagesValidatorHelper

  validates :name, presence: { message: 'title must not empty.' }, uniqueness: true
  validates :description, presence: true
  validates :category, presence: true
  validates :images, presence: true, on: [:create]
  validates :location, presence: true
  validates :user, presence: true

  belongs_to :user
  has_many :images, as: :imageable, dependent: :destroy
  has_many :ratings, as: :rateable, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :views, as: :viewable, dependent: :destroy
  has_many :pins, as: :pinnable, dependent: :destroy
  has_many :notifications, as: :notifiable, dependent: :destroy
  
  geocoded_by :location   # can also be an IP address
  after_validation :geocode          # auto-fetch coordinates

  CATEGORIES = %w(native travel souvenier resort restaurant itinerary park mountain beach underwater culture cave farm people living story house food adventure street bar delicacy fiesta festival event)

  def recent
    limit(8)
  end

  def self.search_all(page, keyword = nil, location = nil)
    includes(:comments, :images, :ratings, :pins, :user)
    .where('(tourisms.name LIKE ? OR tourisms.description LIKE ?) AND tourisms.location LIKE ?', "%#{keyword}%", "%#{keyword}%", "%#{location}%")
    .paginate(page: page)
  end
end
