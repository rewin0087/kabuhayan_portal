module FriendlyParamHelper
  extend ActiveSupport::Concern

  included do 
    before_save :update_slug
    friendly_id :name, use: [:slugged, :finders, :history]
  end
  
  def to_param
    slug
  end

  def update_slug
    token = SecureRandom.hex(5)
    self.slug = "#{token} #{name} #{id} ".parameterize
  end
end