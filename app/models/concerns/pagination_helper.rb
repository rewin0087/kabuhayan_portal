module PaginationHelper
  extend ActiveSupport::Concern
  
  included do 
    default_scope { order('updated_at DESC') }
    self.per_page = 5
  end
end