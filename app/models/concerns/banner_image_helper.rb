module BannerImageHelper
  extend ActiveSupport::Concern

  included do 
  	# before_save :remove_cache_image
  end

  def banner_image_url
  	# return self.cache_banner_image_url unless self.cache_banner_image_url.blank?
  	# update_attributes(cache_banner_image_url: banner_image.url)
   #  self.cache_banner_image_url
   $mobile.blank? ? banner_image.url : medium_url
  end

  def medium_url
  	# return self.cache_medium_banner_image_url unless self.cache_medium_banner_image_url.blank?
  	# update_attributes(cache_medium_banner_image_url: banner_image.medium.url)
   #  self.cache_medium_banner_image_url
   banner_image.medium.url
  end

  def remove_cache_image
  	unless banner_image == banner_image_was
  		self.cache_banner_image_url = nil
  		self.cache_medium_banner_image_url = nil
  	end
  end
end