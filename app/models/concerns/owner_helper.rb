module OwnerHelper
  extend ActiveSupport::Concern

  def owner?(current_user)
    self.user == current_user
  end
end