module ImagesValidatorHelper
  extend ActiveSupport::Concern
  
  included do
    MAX_UPLOAD = 6
    MAX_FILESIZE = 0.3.megabytes.to_i / 1000 # 300kb
    validate :images_exceeded?
    validate :images_file_size_exceeded?
  end

  def images_file_size_exceeded?
    images_file_size_exceeded = images.select {|image| (image.file_name.size.to_f / 1024) >  MAX_FILESIZE }
    unless images_file_size_exceeded.empty?
      errors.add(:images, " file size exceeded to #{MAX_FILESIZE}kb, please upload images not exceeding #{MAX_FILESIZE}kb")
      original_images
    end
  end

  def images_exceeded?
    if images.length > MAX_UPLOAD
      errors.add(:images, "maximum upload is #{MAX_UPLOAD}")
      original_images
    end
  end

  private
    def original_images
      self.images = images.select{|e| e.id? }
    end
end