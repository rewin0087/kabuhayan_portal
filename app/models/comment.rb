class Comment < ActiveRecord::Base
	
  validates :message, presence: true
  validates :user, presence: true

  after_create :notify_commentable_users

  belongs_to :commentable, polymorphic: true
  belongs_to :user
  has_many :ratings, as: :rateable

  def notify_commentable_users
    notifications_params = self.commentable.comments.map{|c| {user: c.user, notifiable: self.commentable, from_user: self.user, category: 'commented'} unless c.user == self.user }.compact.uniq
    notifications_params << {user: self.commentable.user, notifiable: self.commentable, from_user: self.user, category: 'commented'} if self.commentable.user != self.user
    self.commentable.notifications.create(notifications_params.uniq)
  end
end
