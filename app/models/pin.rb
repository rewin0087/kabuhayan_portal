class Pin < ActiveRecord::Base
  include PaginationHelper
  include OwnerHelper
  
  after_create :notify_pinnable_owner

  default_scope { order('updated_at DESC') }
  
  belongs_to :pinnable, polymorphic: true
  belongs_to :user

  # polymorphic reverse relationship
  belongs_to :community, :foreign_key => 'pinnable_id'
  belongs_to :product, :foreign_key => 'pinnable_id'
  belongs_to :tourism, :foreign_key => 'pinnable_id'
  belongs_to :shop, :foreign_key => 'pinnable_id'

  def pinnable_resource
    self.send(type)
  end

  def type
    pinnable_type.downcase
  end

  def notify_pinnable_owner
    Notification.create(user: self.pinnable.user, notifiable: self.pinnable, from_user: self.user, category: 'pinned') unless pinnable.user == user
  end
end
