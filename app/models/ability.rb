class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, Tourism
    can :read, Community
    can :read, Product
    can :read, Comment
    can :read, Pin
    can :read, Rating
    
    can :create, Order
    can :create, Shop
    can :create, Tourism
    can :create, Community 
    can :create, Comment
    can :create, Pin
    can :create, Rating

    can :mine, Order
    can :read, Order do |order|
      order.user == user || order.owner?(user)
    end

    can :update, Order do |order|
      (order.user == user || order.owner?(user)) && order.status != 'cancelled' && order.status != 'rejected'
    end

    can :destroy, Order do |order|
      order.user == user
    end

    can :read, Shop do |shop|
      if shop.owner?(user)  
          true
      elsif (not shop.owner?(user)) && shop.open?
          true
      end
    end

    can :update, Shop do |shop|
      shop.owner?(user)    
    end

    can :destroy, Shop do |shop|
      shop.owner?(user)    
    end

    can :new, Product do |product|
      product.owner?(user)    
    end

    can :create, Product do |product|
      product.owner?(user)    
    end

    can :update, Product do |product|
      product.owner?(user)    
    end

    can :destroy, Product do |product|
      product.owner?(user)    
    end

    can :update, Tourism do |tourism|
      tourism.owner?(user)
    end

    can :destroy, Tourism do |tourism|
      tourism.owner?(user)
    end

    can :update, Community do |community|
      community.owner?(user)
    end

    can :destroy, Community do |community|
      community.owner?(user)
    end

    can :destroy, Pin do |pin|
      pin.owner?(user)
    end

  end
end
