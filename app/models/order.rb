class Order < ActiveRecord::Base
  include PaginationHelper
  
  before_create :set_reference_number
  after_update :clone_to_history
  after_update :notify_users
  before_update :set_payment_reference_number_when_status_is_set_to_payed

  validates :status, presence: true
  validates :remark, presence: true
  validates :product, presence: true
  validates :user, presence: true
  
  belongs_to :product
  belongs_to :user

  has_many :order_histories, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :notifications, as: :notifiable, dependent: :destroy

  STATUSES_AVAILABLE_FOR_OWNERS = %w(rejected accepted payed processing delivering delivered owner_followup)
  STATUSES_AVAILABLE_FOR_BUYERS = %w(cancelled delivery_received buyer_followup)
  STATUSES = STATUSES_AVAILABLE_FOR_OWNERS + STATUSES_AVAILABLE_FOR_BUYERS << 'request_sent'

  STATUSES.each do |status|
    define_method "#{status}?" do
      self.status == status
    end
  end
  
  def owner?(current_user)
    self.product.shop.user == current_user
  end

  def buyer?(current_user)
    self.user == current_user
  end

  def set_reference_number
    self.reference_number = DateTime.now.to_i
  end

  def set_payment_reference_number_when_status_is_set_to_payed
    self.payment_reference_number = "#{DateTime.now.to_i}#{SecureRandom.hex(5)}" if status == 'payed'
  end

  def clone_to_history
    order_histories.create(status: self.status, remark: self.remark)
  end

  def self.for_buyers(owner, page, keyword = nil)
    joins(product: [:shop]).where("shops.user_id" => owner.id).where('orders.reference_number LIKE ?', "%#{keyword}%").paginate( {page: page})
  end

  def self.for_current_user(user, page, keyword = nil)
    joins(:product).where(user: user).where('orders.reference_number LIKE ?', "%#{keyword}%").paginate( {page: page})
  end

  def notify_users
    params = { notifiable: self }
    if STATUSES_AVAILABLE_FOR_OWNERS.include?(self.status)
      params.merge!(user: self.user)
      params.merge!(from_user: self.product.user)
      params.merge!(category: 'update_order_status_from_owner')
    elsif STATUSES_AVAILABLE_FOR_BUYERS.include?(self.status)
      params.merge!(user: self.product.user)
      params.merge!(from_user: self.user)
      params.merge!(category: 'update_order_status_from_buyer')
    elsif status == 'request_sent'
      params.merge!(user: self.product.user)
      params.merge!(from_user: self.user)
      params.merge!(category: 'sent_order_request')
    end   

    self.notifications.create(params)
  end
end
