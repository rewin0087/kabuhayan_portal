class Notification < ActiveRecord::Base
  include PaginationHelper
  
  after_create :deliver_notification

  belongs_to :notifiable, polymorphic: true
  belongs_to :user
  belongs_to :from_user, class_name: 'User', foreign_key: 'from_user'

  def comment?
  	self.category == 'commented'
  end

  def post?
  	self.category == 'posted'
  end

  def pin?
    self.category == 'pinned'
  end

  def thumbsup?
    self.category == 'thumbs up'
  end

  def thumbsdown?
    self.category == 'thumbs down'
  end

  def new_order?
    self.category == 'sent_order_request'
  end

  def order_update_from_owner?
    self.category == 'update_order_status_from_owner'
  end

  def order_update_from_buyer?
    self.category == 'update_order_status_from_buyer'
  end

  def message
    if order_update_from_buyer?
      gender = from_user.gender == 'male' ? 'his' : 'her'
      return "#{from_user.first_name} changed #{gender} order status"
    end

    if order_update_from_owner?
      return "#{from_user.first_name} changed the status of your order"
    end

    return "#{from_user.first_name} #{category.humanize}" if new_order?
  	return "#{from_user.first_name} #{category} at #{notifiable.try(:name) or 'your order'}" if post? || comment?
    return "#{from_user.first_name} #{category} your #{notifiable.try(:name) or 'post'}" if pin? || thumbsdown? || thumbsup?
  end

  def self.seen?(user, resource)
    where(user: user, notifiable: resource)
  end

  def self.seen!(user, resource)
    unless (notifications = seen?(user, resource)).blank?
      notifications.update_all(seen: true)
    end
  end

  def deliver_notification
    html = ApplicationController.new.render_to_string(partial: 'notifications/notification', locals: { notification: self } )
    url = File.join('/',"#{notifiable_type.downcase.pluralize}", "#{notifiable.id}")
    WebsocketRails[:notifications].trigger 'new', { raw: self, html: html, total: self.user.notifications.size, content: message, url: url }
  end
end
