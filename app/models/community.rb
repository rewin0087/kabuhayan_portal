class Community < ActiveRecord::Base
  mount_uploader :banner_image, BannerImageUploader
  
  extend FriendlyId
  include FriendlyParamHelper
  include OwnerHelper
  include PaginationHelper
  include BannerImageHelper
  
  validates :name, presence: true, uniqueness: true
  validates :description, presence: true
  validates :category, presence: true
  validates :location, presence: true
  validates :banner_image, presence: true, on: [:create]
  validates :user, presence: true
  
  belongs_to :user
  has_many :posts, dependent: :destroy
  has_many :ratings, as: :rateable, dependent: :destroy
  has_many :views, as: :viewable, dependent: :destroy
  has_many :pins, as: :pinnable, dependent: :destroy
  has_many :notifications, as: :notifiable, dependent: :destroy
  
  CATEGORIES = %w(farmers fishermans businessman tourists explorer adventurer blogger local foreign hikers)

  def self.random(limit = 3)
    limit = 1 unless $mobile.blank?
    self.where('banner_image is not null').shuffle[0...limit]
  end
end
