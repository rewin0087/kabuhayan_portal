class Shop < ActiveRecord::Base
  mount_uploader :banner_image, BannerImageUploader
  
  extend FriendlyId
  include FriendlyParamHelper
  include OwnerHelper
  include PaginationHelper
  include BannerImageHelper

  validates :name, presence: true, uniqueness: true
  validates :description, presence: true
  validates :category, presence: true
  validates :location, presence: true
  validates :mobile, 
    presence: true, 
    numericality: { only_integer: true, message: ' number is not a number' }, 
    length: { is: 11, message: 'Mobile number should be 11 digits' }
  validates :telephone, 
    allow_blank: true,
    numericality: { only_integer: true, message: ' number is not a number' }, 
    length: { is: 10, message: 'Telephone number should be 10 digits' }
  validates :banner_image, presence: true, on: [:create]
  validates :status, presence: true
  validates :user, presence: true
  
  belongs_to :user
  has_many :products, dependent: :destroy
  has_many :ratings, as: :rateable, dependent: :destroy
  has_many :pins, as: :pinnable, dependent: :destroy
  has_many :views, as: :viewable, dependent: :destroy
  
  CATEGORIES = %w(native travel delicacy vegetable fruit souvenier resort restaurant market wet_market)
  STATUSES = %w(open closed inactive)
  
  STATUSES.each do |status|
    define_method "#{status}?" do
      self.status == status
    end
  end

  def recent
    limit(8)
  end

  def contact_number
    return "#{mobile} / #{telephone}" unless mobile.blank? || telephone.blank?

    mobile.presence or telephone.presence
  end
end
