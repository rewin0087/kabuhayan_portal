class Image < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true
  mount_uploader :file_name, ImageUploader

  def image_url
    # return self.cache_image_url unless self.cache_image_url.blank?
    # update_attributes(cache_image_url: file_name.url)
    # self.cache_image_url
    $mobile.blank? ?  file_name.url : medium_url
  end

  def medium_url
  	# return self.cache_medium_url unless self.cache_medium_url.blank? 
  	# update_attributes(cache_medium_url: file_name.medium.url)
  	# self.cache_medium_url
    file_name.medium.url
  end

end
