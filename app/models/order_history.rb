class OrderHistory < ActiveRecord::Base
  belongs_to :order
  default_scope { order('updated_at DESC') }
end
