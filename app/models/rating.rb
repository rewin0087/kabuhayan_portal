class Rating < ActiveRecord::Base
  after_create :notify_ratable_owner
  belongs_to :rateable, polymorphic: true
  belongs_to :user

  def notify_ratable_owner
    unless rateable.user == user
    	cat = self.category == 'negative' ? 'thumbs down' : 'thumbs up'
      Notification.create(user: self.rateable.user, notifiable: self.rateable, from_user: self.user, category: cat)
    end
  end
end
