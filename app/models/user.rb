class User < ActiveRecord::Base
  extend FriendlyId
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, :omniauth_providers => [:facebook]

  before_save :update_full_name
  before_save :update_slug
  friendly_id :full_name, use: [:slugged, :finders, :history]

  validates :email, presence: true, uniqueness: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :gender, presence: true
  validates :uid, uniqueness: true, unless: :facebook_provider?

  has_many :shops, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :communities, dependent: :destroy
  has_many :tourisms, dependent: :destroy
  has_many :views, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :pins, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :notifications, -> { where(seen: nil).order('updated_at DESC') }, dependent: :destroy
  
  def facebook_provider?
    self.provider.blank?
  end

  def update_slug
    self.slug = "#{full_name.parameterize}-#{id}"
  end

  def to_param
    slug
  end

  def update_full_name
    self.full_name = "#{self.first_name.humanize} #{self.last_name.humanize}"
  end

  def self.find_for_oauth(auth)
    user = self.find_by_uid(auth[:uid])
    update_info = {
      uid: auth[:uid], 
      full_name: auth[:info][:name],
      first_name: auth[:info][:first_name],
      last_name: auth[:info][:last_name],
      image_url: auth[:info][:image],
      gender: auth[:extra][:raw_info][:gender],
      provider: auth[:provider],
      email: auth[:info][:email]
    }

    if user.nil?
      update_info[:password] = Devise.friendly_token[0,20]
      user = new(update_info)
      user.save!
    else
      user.update_attributes(update_info)
    end

    user
  end
end
