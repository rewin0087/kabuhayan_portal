class Post < ActiveRecord::Base
  include PaginationHelper

  after_create :notify_community_users

  validates :content, presence: true
  validates :user, presence: true
  validates :community, presence: true

  belongs_to :community
  belongs_to :user
  has_many :ratings, as: :rateable, dependent: :destroy

  def notify_community_users
    notifications_params = self.community.posts.map{|p| {user: p.user, notifiable: self.community, from_user: self.user, category: 'posted'} unless p.user == self.user }.compact.uniq
    notifications_params << {user: self.community.user, notifiable: self.community, from_user: self.user, category: 'posted'} if self.community.user != self.user
    self.community.notifications.create(notifications_params.uniq)
  end
end
