class Product < ActiveRecord::Base
  extend FriendlyId
  include FriendlyParamHelper
  include PaginationHelper
  include ImagesValidatorHelper
  include OwnerHelper
  
  validates :name, presence: true, uniqueness: true
  validates :description, presence: true
  validates :category, presence: true
  validates :location, presence: true
  validates :images, presence: true, on: [:create]
  validates :price, presence: true, numericality: true
  validates :status, presence: true
  validates :shop, presence: true
  
  belongs_to :shop
  has_many :images, as: :imageable, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :ratings, as: :rateable, dependent: :destroy
  has_many :views, as: :viewable, dependent: :destroy
  has_many :pins, as: :pinnable, dependent: :destroy
  has_many :orders
  has_many :notifications, as: :notifiable, dependent: :destroy
  has_one :user, :through => :shop
  
  CATEGORIES = %w(food beverage vegetable fruits souvenier delicacy accomodation travel other seafood meat)
  STATUSES = %w(available sold inactive)

  STATUSES.each do |status|
    define_method "#{status}?" do
      self.status == status
    end
  end
  
  def self.search_all(page, keyword = nil, location = nil)
    includes(:comments, :ratings, :pins, :images, :shop, :user)
    .where(status: :available)
    .where('(products.name LIKE ? OR products.description LIKE ?) AND products.location LIKE ?', "%#{keyword}%", "%#{keyword}%", "%#{location}%")
    .paginate(page: page)
  end
end
