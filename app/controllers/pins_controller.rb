class PinsController < ApplicationController
  load_and_authorize_resource

  def create
    klass, id = params[:type].constantize, params[:type_id]

    model = klass.find(id)
    pin = model.pins.create(user: current_user)
    index = params[:type].downcase

    WebsocketRails[:pins].trigger 'new', { raw: pin, user: current_user.id, pins: model.pins.size, element: "#{index}-pin-#{model.id}" }

    respond_to do |format|
      format.js
      format.html { return redirect_to(:back) }
    end
  end

  def destroy
    @pin.destroy
    WebsocketRails[:pins].trigger 'destroy', { raw: @pin, user: current_user.id, pins: @pin.pinnable_resource.pins.size, element: "#{@pin.type}-pin-#{@pin.pinnable_id}" }
    respond_to do |format|
      format.js
      format.html { return redirect_to(:back) }
    end
  end
end
