class PostsController < ApplicationController
  def create
    @community = Community.find(params[:community_id])
    @post = @community.posts.new(params.require(:post).permit(post_params))
    @post.user = current_user
    
    if @post.valid?
      @post.save

      html_to_public = render_to_string(partial: 'post', locals: { post: @post, current_user: nil })
      html_to_owner = render_to_string(partial: 'post', locals: { post: @post, current_user: current_user })
      WebsocketRails[:posts].trigger 'new', { raw: @post, html: { owner: html_to_owner, public: html_to_public }, element: "posts-community-#{@community.id}" }
    end

    flash.now[:error] = @post.errors.full_messages.first

    respond_to do |format|
      format.js
      format.html { return redirect_to(:back) }
    end
  end

  def show
    @post = Post.find(params[:id])
    notify_seen! @post
    redirect_to @post.community
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    WebsocketRails[:posts].trigger 'destroy', { raw: @post }

    respond_to do |format|
      format.js
      format.html { return redirect_to(:back) }
    end
  end

  private 
    def post_params
      %w(content)
    end
end
