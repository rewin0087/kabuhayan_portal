class CommentsController < ApplicationController
  def create
    klass, id, message = params[:type].constantize, params[:type_id], params[:message]

    model = klass.find(id)
    @comment = model.comments.create(user: current_user, message: message)
    index = params[:type].downcase
    
    if @comment.errors.present?
      flash.now[:error] = @comment.errors.full_messages.first 

      render status: 500
    else
      html_to_public = render_to_string(partial: 'comment', locals: { comment: @comment, current_user: nil })
      html_to_owner = render_to_string(partial: 'comment', locals: { comment: @comment, current_user: current_user })
      WebsocketRails[:comments].trigger 'new', { raw: @comment, html: { owner: html_to_owner, public: html_to_public }, comments: model.comments.size, element: "comments-#{index}-#{model.id}"}
    end

    respond_to do |format|
      format.js
      format.html { return redirect_to(:back) }
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    WebsocketRails[:comments].trigger 'destroy', { raw: @comment }
    
    respond_to do |format|
      format.js
      format.html { return redirect_to(:back) }
    end
  end
end
