class HomeController < ApplicationController
  def index
    @products = Product.search_all(params[:page], params[:keyword], params[:location])
    
    respond_to do |format|
      format.html { @communities = Community.random }
      format.js { flash.now[:error] = 'No more feeds to load.' if @products.blank? }
    end
  end
end
