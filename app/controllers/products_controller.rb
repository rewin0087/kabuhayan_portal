class ProductsController < ApplicationController
  before_filter :find_shop, only: [:new, :create, :edit, :show, :update, :destroy]
  before_filter :find_product, only: [:edit, :show, :update, :destroy]
  load_and_authorize_resource param_method: :sanitize_params, except: [:new, :create]
  
  def index
  end

  def show
    notify_seen! @product
    visit_resource! @product
    @comment = Comment.new
  end

  def new
    return render_404 unless @shop.owner?(current_user)
    @product = Product.new
  end

  def edit
  end

  def create
    return render_404 unless @shop.owner?(current_user)
    @product = @shop.products.new(params.require(:product).permit(product_params))
    @product.images.new(params[:product][:images].map{|i| {file_name: i} }) unless params[:product][:images].blank?
    
    if @product.valid?
      @product.save
      flash.now[:success] = "Successfully Added new #{@product.name}."
      return redirect_to [@shop, @product], turbolinks: true
    end

    flash[:error] = @product.errors.full_messages.first
    render :new
  end

  def update
    @product.images.new(params[:product][:images].map{|i| {file_name: i} }) unless params[:product][:images].blank?
    
    if @product.update_attributes(params.require(:product).permit(product_params))
      flash.now[:success] = "Successfully updated #{@product.name}."
      return redirect_to [@shop, @product], turbolinks: true
    end

    flash.now[:error] = @product.errors.full_messages.first
    render :edit
  end

  def destroy
  end

  private
    def find_shop
      @shop = Shop.includes(:products).find(params[:shop_id]) if params[:shop_id]
    end

    def find_product
      @product = Product.find(params[:id])
    end

    def sanitize_params
      params.require(:product).permit(product_params)
    end

    def product_params
      %w(name description category price location images status)
    end
end
