class CommunitiesController < ApplicationController
  load_and_authorize_resource param_method: :sanitize_params
  before_filter :find_all_posts, only: [:show]

  def show
    respond_to do |format|
      format.html do
        notify_seen! @community
        visit_resource! @community
      end
      format.js { flash.now[:error] = 'No more feeds to load.' if @posts.blank? }
    end
  end

  def new
    @community = Community.new
  end

  def edit
  end

  def create
    @community.user = current_user

    if @community.valid?
      @community.save
      find_all_posts
      return redirect_to @community, turbolinks: true
    end

    flash.now[:error] = @community.errors.full_messages.first
    render :new
  end

  def update
    if @community.update_attributes(params.require(:community).permit(community_params))
      find_all_posts
      return redirect_to @community, turbolinks: true
    end

    flash.now[:error] = @community.errors.full_messages.first
    render :edit
  end

  def destroy
  end

  private
    def find_all_posts
      @posts = Post.where(community: @community).paginate(:page => params[:page])
      @next_page =  params[:page].nil? ? 2 : params[:page].to_i + 1
      @post = Post.new
    end

    def sanitize_params
      params.require(:community).permit(community_params)
    end

    def community_params
      %w(name description category location banner_image)
    end
end
