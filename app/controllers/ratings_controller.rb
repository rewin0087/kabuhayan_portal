class RatingsController < ApplicationController
  load_and_authorize_resource
  
  def create
    klass, id, category = params[:type].constantize, params[:type_id], params[:category]

    model = klass.find(id)
    
    if not (@rate = model.ratings.find_by_user_id_and_category(current_user, category)).blank?
      @channel = 'destroy'
      @rate.destroy
    else
      @channel = 'new'
      @rate = model.ratings.create(user: current_user, category: category)
    end

    index = params[:type].downcase

    WebsocketRails[:ratings].trigger @channel, { raw: @rate, user: current_user.id, ratings: model.ratings.where(category: category).size, element: "#{index}-rate-#{category}-#{model.id}" }

    respond_to do |format|
      format.js
      format.html { return redirect_to(:back) }
    end
  end

  def destroy
  end
end
