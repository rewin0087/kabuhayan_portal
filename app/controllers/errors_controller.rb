class ErrorsController < ApplicationController
  skip_before_filter :authenticate_user!
  layout 'dialogs'

  def not_found
  end

  def server_error
  end

  def unprocessable
  end

  protected 

  def current_user
    nil
  end
end
