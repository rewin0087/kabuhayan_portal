class TourismsController < ApplicationController
  load_and_authorize_resource param_method: :sanitize_params
  
  def index
    @tourisms = Tourism.search_all(params[:page], params[:keyword], params[:location])
    
    respond_to do |format|
      format.html
      format.js { flash.now[:error] = 'No more feeds to load.' if @tourisms.blank? }
    end
  end

  def show
    notify_seen! @tourism
    visit_resource! @tourism
  end

  def create
    @tourism.user = current_user
    @tourism.images.new(params[:tourism][:images].map{|i| {file_name: i } }) unless params[:tourism][:images].blank?

    if @tourism.valid?
      @tourism.save
      flash.now[:success] = "Successfully created new contribution."
      @comment = Comment.new
      return redirect_to @tourism, turbolinks: true
    end

    flash.now[:error] = @tourism.errors.full_messages.first
    render :new
  end

  def update
    @tourism.images.new(params[:tourism][:images].map{|i| {file_name: i } }) unless params[:tourism][:images].blank?

    if @tourism.update_attributes(params.require(:tourism).permit(tourism_params))
      flash.now[:success] = "Successfully updated #{@tourism.name}."
      @comment = Comment.new
      return redirect_to @tourism, turbolinks: true
    end

    flash.now[:error] = @tourism.errors.full_messages.first
    render :edit
  end

  def edit
  end

  def new
    @tourism = Tourism.new
  end

  def destroy
  end

  private

    def sanitize_params
      params.require(:tourism).permit(tourism_params)
    end

    def tourism_params
      %w(name description category location images)
    end
end
