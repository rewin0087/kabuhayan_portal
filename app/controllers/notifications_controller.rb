class NotificationsController < ApplicationController
  def index
  	@notifications = Notification.includes(:notifiable, :from_user).where(user: current_user).paginate(page: params[:page])
  	respond_to do |format|
      format.html
      format.js { flash.now[:error] = 'No more Notifications to load.' if @notifications.blank? }
    end
  end
end
