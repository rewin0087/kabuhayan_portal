class ImageController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:banner, :show]

  def banner
    parse_image('banners', params[:size])
  end

  def show
    parse_image('images', params[:size])
  end

  def destroy
    @image = Image.find(params[:id])
    @image.destroy
  end

  private
    def parse_image(folder, size = nil)
      location = Rails.root.join('uploads', folder, "#{params[:image]}.#{params[:format]}")
      return render_404 unless File.exist?(location)
      image = MiniMagick::Image.open(location)

      if size.nil?
        image = File.open(image.path, 'rb') {|f| f.read}
      else
        
        image.resize "#{size}x#{size}"
        image.contrast
        image = File.open(image.path, 'rb') {|f| f.read}
      end

      response.header['Content-type'] = params[:format]
      render :text => image
    end
end
