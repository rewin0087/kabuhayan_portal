class OrdersController < ApplicationController
  load_and_authorize_resource param_method: :sanitize_params, except: [:create]

  def index
    @orders = Order.for_buyers(current_user, params[:page], params[:keyword])
  end

  def show
    notify_seen! @order
  end

  def create
    @product = Product.find(params[:product])
    @order = Order.create(user: current_user, product: @product, status: 'request_sent', remark: request_sent_remark )
    
    if @order.valid?
      @order.save
    else
      flash.now[:error] = @order.errors.full_messages.first
    end
    
    respond_to do |format|
      format.js
      format.html { return redirect_to(:back) }
    end
  end

  def update
    if @order.update_attributes(params.require(:order).permit(order_params))
      order_details_for_buyer = order_details(@order.user)
      order_details_for_owner = order_details(@order.product.user)
      WebsocketRails[:orders].trigger 'update', { raw: @order, html: { buyer: order_details_for_buyer, owner: order_details_for_owner}, element: "order-#{@order.id}"}
    else
      flash.now[:error] = @order.errors.full_messages.first
    end

    respond_to do |format|
      format.js
      format.html { return redirect_to(:back) }
    end
  end

  def destroy
    @product = @order.product
    @order.destroy

    flash.now[:error] = @order.errors.full_messages.first unless @order.errors.blank?
  end

  def mine
    @orders = Order.for_current_user(current_user, params[:page], params[:keyword])

    respond_to do |format|
      format.html
      format.js { flash.now[:error] = 'No more Orders to load.' if @orders.blank? }
    end
  end

  private
    def request_sent_remark
      # FIXME: move to locales
      "Hi I am interested to buy your #{@product.name}, please send me a feedback."
    end

    def sanitize_params
      params.require(:order).permit(order_params)
    end

    def order_params
      %w(status remark)
    end

    def order_details(user)
      render_to_string(partial: 'details', locals: {user: user})
    end
end
