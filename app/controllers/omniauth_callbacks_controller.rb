class OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    user = User.find_for_oauth(env['omniauth.auth'])
    sign_in(user)
    redirect_to root_path, notice: 'Welcome to Kabuhayan Portal: No. 1 Livelihood Site in the Philippines'
  end
end
