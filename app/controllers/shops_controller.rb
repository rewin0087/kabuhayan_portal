class ShopsController < ApplicationController
  load_and_authorize_resource param_method: :sanitize_params

  def new
    @shop = Shop.new
  end

  def edit 
  end

  def show
    visit_resource! @shop
  end

  def create
    @shop.user = current_user

    if @shop.valid?
      @shop.save
      flash.now[:success] = "Successfully Created #{@shop.name}."
      return redirect_to @shop, turbolinks: true
    end

    flash.now[:error] = @shop.errors.full_messages.first
    render :new
  end

  def update

    if @shop.update_attributes(params.require(:shop).permit(shop_params))
      flash.now[:success] = "Sccuessfully updated #{@shop.name}."
      return redirect_to @shop, turbolinks: true
    end

    flash.now[:error] = @shop.errors.full_messages.first
    render :edit
  end

  def destroy
  end

  private
    def sanitize_params
      params.require(:shop).permit(shop_params)
    end

    def shop_params
      %w(name description category location mobile telephone banner_image status)
    end
end
