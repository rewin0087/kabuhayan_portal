class AboutController < ApplicationController
  skip_before_filter :authenticate_user!
  layout 'dialogs'

  def show
  end

  def privacy_policy
  end

  def contact_us
  end
end
