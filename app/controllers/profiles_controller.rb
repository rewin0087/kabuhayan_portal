class ProfilesController < ApplicationController
  before_filter :find_profile, only: [:edit, :show, :update, :show, :communities, :shops, :pins, :update_password, :map]

  def show
    @tourisms = Tourism.includes(:comments, :ratings, :pins, :images).where(user: @current_profile).paginate(page: params[:page])
    respond_to do |format|
      format.html
      format.js { flash.now[:error] = 'No more feeds to load.' if @tourisms.blank? }
    end
  end

  def communities
    @communities = Community.includes(:posts, :ratings, :pins).where(user: @current_profile).paginate(page: params[:page])

    respond_to do |format|
      format.html
      format.js { flash.now[:error] = 'No more feeds to load.' if @communities.blank? }
    end
  end

  def shops
    @shops = Shop.includes(:ratings, :pins, :products).where(user: @current_profile).paginate(page: params[:page])

    respond_to do |format|
      format.html
      format.js { flash.now[:error] = 'No more feeds to load.' if @shops.blank? }
    end
  end

  def pins
    @pins = Pin.includes(:community, :product, :tourism).where(user: @current_profile).paginate(page: params[:page])

    respond_to do |format|
      format.html
      format.js { flash.now[:error] = 'No more feeds to load.' if @pins.blank? }
    end
  end

  def map
    marker = ActionController::Base.helpers.asset_path('marker.png')
    @tourisms = @current_profile.tourisms.map{|t| {lat: t.latitude, lng: t.longitude, picture: {url: marker, height: 60, width: 60 }, infowindow: t.name.humanize }}.to_json
  end

  def edit
  end

  def update
		@current_profile.update_attributes(params.require(:user).permit(user_params))

		if save_user!(params)
			flash.now[:success] = "Sccuessfully updated #{@current_profile.first_name}."
			return render :show
		end

    flash.now[:error] = @current_profile.errors.full_messages.first
    render :edit
  end

  def update_password
    if user = params[:user]
      if current_user.valid_password? user[:current_password]
        current_user.reset_password!(user[:password], user[:password_confirmation])

        if current_user.errors.empty?
          flash.now[:success] = 'Password has successfully updated.'
          sign_in(current_user, bypass: true)
        else
          flash.now[:error] = current_user.errors.full_messages.first
        end
      else
        flash.now[:error] = 'Please Enter your correct password'
      end
    end
    render :edit
  end

  private
    def find_profile
      @current_profile = User.find(profile_id)
    end

    def profile_id
      params[:id] or params[:profile_id]
    end

    def save_user!(params)
      if @current_profile.valid?
        @current_profile.save
        return true
      end
      false
    end

    def user_params
      %w(first_name last_name gender email)
    end
end