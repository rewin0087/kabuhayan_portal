MiniMagick.configure do |config|
  config.validate_on_create = true
  config.validate_on_write = true
end