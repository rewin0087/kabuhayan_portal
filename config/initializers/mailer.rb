Rails.application.config.action_mailer.tap do |c|
  c.perform_deliveries = eval(ENV['PERFORM_DELIVERIES'])
  c.raise_delivery_errors = false
  c.default_url_options = { host: ENV['HOST'] }
  c.delivery_method = ENV['DELIVERY_METHOD'].to_sym
  c.smtp_settings = {
    address: 'smtp.gmail.com',
    port: 587,
    domain: 'mail.google.com',
    authentication: "plain",
    enable_starttls_auto: true,
    user_name: ENV['SMTP_USER_NAME'],
    password: ENV['SMTP_PASSWORD']
  }
end

Devise.mailer_sender = ENV['DEFAULT_SENDER']