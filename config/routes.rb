Rails.application.routes.draw do
  get 'image/banner/:image', to: 'image#banner', as: 'image_banner'
  get 'image/:image', to: 'image#show', as: 'image'
  get 'home', to: 'home#index', as: 'home'
  get 'about', to: 'about#show', as: 'about'
  get 'privacy_policy', to: 'about#privacy_policy', as: 'privacy_policy'
  get 'contact_us', to: 'about#contact_us', as: 'contact_us'
  
  devise_for :users, path: '', path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    password: 'password',
    confirmation: 'verification',
    registration: 'register'
  }, controllers: {
    omniauth_callbacks: 'omniauth_callbacks'
  }

  resources :notifications, only: [:index]
  resources :image, only: [:destroy]
  resources :ratings, only: [:create]
  resources :comments, only: [:create, :destroy]
  resources :pins, only: [:create, :destroy]
  resources :products, only: [:show]
  resources :posts, only: [:show, :destroy]
  resources :tourisms
  resources :profiles, except: [:index, :destroy, :create] do
    get 'communities'
    get 'tourisms'
    get 'shops'
    get 'pins'
    get 'map'
    put 'update_password'
  end

  resources :communities, except: [:index] do 
    resources :posts, only: [:create, :destroy]
  end

  resources :orders, except: [:new, :edit] do 
    collection do 
      get 'mine'
    end
  end
  
  resources :shops, except: [:index] do
    resources :products
  end


  root 'home#index'
  match '/404', to: 'errors#not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#server_error', via: :all
end