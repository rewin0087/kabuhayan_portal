require 'rails_helper'

RSpec.describe ImageController, :type => :controller do

  describe "GET banner" do
    it "returns http success" do
      get :banner
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET show" do
    it "returns http success" do
      get :show
      expect(response).to have_http_status(:success)
    end
  end

end
